# Specify Node version
FROM node:18 AS build-stage

# Create the app directory
WORKDIR /app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
COPY package*.json ./

RUN npm install

# Copy everything else on the project
COPY . .

# build app for production with minification
RUN npm run build



# PRODUCTION
FROM nginx AS production-stage

RUN mkdir /app

COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY nginx.conf /etc/nginx/nginx.conf

# Expose the port we want to use
EXPOSE 80

